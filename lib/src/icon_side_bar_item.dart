
import 'package:flutter/cupertino.dart';

class IconSideBarItem {

  final IconData icon;
  final String label;

  const IconSideBarItem({
    this.icon,
    this.label
  }): assert(icon != null),
      assert(label != null);

}



