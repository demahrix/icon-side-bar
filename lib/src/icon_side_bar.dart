import 'package:flutter/material.dart';
import 'package:icon_side_bar/src/icon_side_bar_item.dart';

class IconSideBar extends StatefulWidget {

  final Widget top;
  final Widget bottom;
  final double width;
  final int initialIndex;
  final List<IconSideBarItem> icons;
  final double iconSize;
  final Color backgroundColor;
  final Color unselectedColor;
  final Color selectedColor;

  IconSideBar({
    this.top,
    this.bottom,
    this.width = 56.0,
    this.initialIndex = 0,
    this.icons,
    this.iconSize = 24.0,
    this.backgroundColor = const Color(0xFF333333),
    this.unselectedColor = Colors.white38,
    this.selectedColor = Colors.white
  }): assert(width != null),
      assert(initialIndex != null && initialIndex > -1),
      assert(icons != null),
      assert(backgroundColor != null),
      assert(unselectedColor != null),
      assert(selectedColor != null);

  @override
  _IconSideBarState createState() => _IconSideBarState(initialIndex);
}

class _IconSideBarState extends State<IconSideBar> {

  int _currentIndex;
  _IconSideBarState(int initialIndex): _currentIndex = initialIndex;

  _change(int index) {
    if (index != _currentIndex) {
      setState(() { _currentIndex = index; });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      decoration: BoxDecoration(
        color: widget.backgroundColor
      ),
      child: Column(
        children: [

          if (widget.top != null)
              widget.top,

          Expanded(child: _getIcons()),

          if (widget.bottom != null)
            widget.bottom

        ],
      ),
    );
  }

  Widget _getIcons() {
    final icons = widget.icons;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(icons.length, (index) {
        final icon = icons[index];
        final bool selected = _currentIndex == index;

        return IconButton(
          iconSize: widget.iconSize,
          icon: Icon(
            icon.icon,
            color: selected ? widget.selectedColor : widget.unselectedColor,
          ),
          onPressed: () => _change(index),
          tooltip: icon.label,
        );
      }),
    );
  }

}
