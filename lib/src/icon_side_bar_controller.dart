
typedef _Listener = void Function(int);

class IconSideBarController {

  int _currentIndex;
  Set<_Listener> _listeners;

  int get index => _currentIndex;

  IconSideBarController({ int initialIndex }): _currentIndex = initialIndex;

  void Function(int) change;

  void listen(_Listener _listener) {
    _listeners.add(_listener);
  }

  bool removeListener(_Listener listener) {
    return _listeners.remove(listener);
  }

  void dispatch(int index) {
    for (final _Listener _listener in _listeners)
      _listener(index);
  }

  void dispose() {
    _listeners.clear();
    _listeners = null;
  }

}
